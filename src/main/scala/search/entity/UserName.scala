package chessfinder
package search.entity

opaque type UserName = String

object UserName extends OpaqueString[UserName]
