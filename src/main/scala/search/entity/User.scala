package chessfinder
package search.entity

import search.entity.ChessPlatform

import search.entity.UserName

final case class User(platform: ChessPlatform, userName: UserName)
